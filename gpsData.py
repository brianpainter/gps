#! /usr/bin/python
# Written by Dan Mandle http://dan.mandle.me September 2012
# License: GPL 2.0
 
import os
from gps import *
import time
import threading
import sqlite3
import signal
from Adafruit_CharLCD import Adafruit_CharLCD

 
gpsd = None #seting the global variable

lcd = Adafruit_CharLCD()

lcd.begin(16,1)
 
os.system('clear') #clear the terminal (optional)
 
class GpsPoller(threading.Thread):
  def __init__(self):
    threading.Thread.__init__(self)
    global gpsd #bring it in scope
    gpsd = gps(mode=WATCH_ENABLE) #starting the stream of info
    self.current_value = None
    self.running = True #setting the thread running to true
 
  def run(self):
    global gpsd
    while gpsp.running:
      gpsd.next() #this will continue to loop and grab EACH set of gpsd info to clear the buffer


 
if __name__ == '__main__':
    gpsp = GpsPoller() # create the thread

    # connect to database
    conn = sqlite3.connect('/home/pi/gps/gpsLog.db')
    cur = conn.cursor()
    try:
        gpsp.start() # start it up
        while True:
            #It may take a second or two to get good data
            #print gpsd.fix.latitude,', ',gpsd.fix.longitude,'  Time: ',gpsd.utc
            os.system('clear')

            #print
            #print ' GPS reading'
            #print '----------------------------------------'
            #print 'latitude    ' , gpsd.fix.latitude
            #print 'longitude   ' , gpsd.fix.longitude
            #print 'time utc    ' , gpsd.utc,' + ', gpsd.fix.time
            #print 'altitude (m)' , gpsd.fix.altitude
            #print 'eps         ' , gpsd.fix.eps
            #print 'epx         ' , gpsd.fix.epx
            #print 'epv         ' , gpsd.fix.epv
            #print 'ept         ' , gpsd.fix.ept
            #print 'speed (m/s) ' , gpsd.fix.speed
            #print 'climb       ' , gpsd.fix.climb
            #print 'track       ' , gpsd.fix.track
            #print 'mode        ' , gpsd.fix.mode
            #print
            #print 'sats        ' , gpsd.satellites
            gps_time = gpsd.utc
            lat = gpsd.fix.latitude
            long = gpsd.fix.longitude

            if gpsd.fix.mode == 3:
                if gpsd.fix.time and gpsd.fix.latitude and gpsd.fix.longitude:
                    data = [gps_time, lat, long]
                    print data
                    cur.execute("INSERT INTO gpsdata (date, latitude, longitude) VALUES(?, ?, ?)", data)
                    conn.commit()
                    lcd.message('lat %s\n' % (str(lat)))
                    lcd.message('long %s' % (str(long)))

            time.sleep(1) #set to whatever
            lcd.clear()

    except (KeyboardInterrupt, SystemExit, signal.signal(signal.TERM)): #when you press ctrl+c
        print "\nKilling Thread..."
        gpsp.running = False
        gpsp.join() # wait for the thread to finish what it's doing
        conn.close()
        print "Done.\nExiting."
